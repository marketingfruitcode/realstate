//
//  Extensions.swift
//  RealEstate
//
//  Created by Muhammad Umair on 18/05/2020.
//  Copyright © 2020 Code Gradients. All rights reserved.
//

import Foundation
import UIKit

enum AppStoryboard : String {
    case Main, Utils, Auth, AddProp
    var shared : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
}

extension UIViewController {
    class var storyboard_id : String {
        return "\(self)"
    }
}

extension UITableViewCell {
    class var identifier: String {
        return "\(self)"
    }
}

extension UICollectionViewCell {
    class var identifier: String {
        return "\(self)"
    }
}

extension UIScrollView {
    func scrollToBottom(animated: Bool) {
        if self.contentSize.height < self.bounds.size.height { return }
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: animated)
    }
}

extension Notification.Name {
    static let currencyUpdated = Notification.Name("currencyUpdated")
    static let didLoadInitialy = Notification.Name("initialLoading")
    static let minePropertyAdded = Notification.Name("minePropertyAdded")
    static let propertyAdded = Notification.Name("propertyAdded")
    static let propertyChanged = Notification.Name("propertyChanged")
    static let propertyRemoved = Notification.Name("propertyRemoved")
}

extension UIView {
    
    var heightConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .height && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    var widthConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .width && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    func with(frame: CGRect) -> UIView {
        let v = UIView(frame: frame)
        v.backgroundColor = self.backgroundColor
        v.tag = self.tag
        return v
    }
}

extension UIView {
    func findViewController() -> UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.findViewController()
        } else {
            return nil
        }
    }
}

public extension UIAppearance {
    @discardableResult
    func style(_ styleClosure: (Self) -> Void) -> Self {
        styleClosure(self)
        return self
    }
}

class ClosureSleeve: NSObject {
    let closure: ()->()

    init (_ closure: @escaping ()->()) {
        self.closure = closure
    }

    @objc func invoke () {
        closure()
    }
}

extension UIButton {
    func addAction(for controlEvents: UIControl.Event = .touchUpInside, _ closure: @escaping ()->()) {
        let sleeve = ClosureSleeve(closure)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
        objc_setAssociatedObject(self, "[\(arc4random())]", sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
    
    static var primary: UIColor {
        return #colorLiteral(red: 0.0862745098, green: 0.3215686275, blue: 0.9411764706, alpha: 1)
    }
}

extension UIImage {
    func resized(withPercentage percentage: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    func resized(toWidth width: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
}

extension Date {
    
    func isEqual(to date: Date, toGranularity component: Calendar.Component, in calendar: Calendar = .current) -> Bool {
        calendar.isDate(self, equalTo: date, toGranularity: component)
    }
    
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    func isBetween(start:Date, end:Date)->Bool {
        return (start.compare(self) == .orderedAscending) && (end.compare(self) == .orderedDescending)
    }
    
    func isInSameYear (date: Date) -> Bool { isEqual(to: date, toGranularity: .year) }
    func isInSameMonth(date: Date) -> Bool { isEqual(to: date, toGranularity: .month) }
    func isInSameDay  (date: Date) -> Bool { isEqual(to: date, toGranularity: .day) }
    func isInSameWeek (date: Date) -> Bool { isEqual(to: date, toGranularity: .weekOfYear) }
    
    var isInThisYear:  Bool { isInSameYear(date: Date()) }
    var isInThisMonth: Bool { isInSameMonth(date: Date()) }
    var isInThisWeek:  Bool { isInSameWeek(date: Date()) }
    
    var isInYesterday: Bool { Calendar.current.isDateInYesterday(self) }
    var isInToday:     Bool { Calendar.current.isDateInToday(self) }
    var isInTomorrow:  Bool { Calendar.current.isDateInTomorrow(self) }
    
    var isInTheFuture: Bool { self > Date() }
    var isInThePast:   Bool { self < Date() }
    
    var year: Int { Calendar.current.component(.year, from: self) }
    var month: Int { Calendar.current.component(.month, from: self) }
    
    var daysUntilToday: Int {
        let components = Calendar.current.dateComponents([.day], from: self, to: Date())
        return components.day ?? 0
    }
}

extension String {
    static func format(strings: [String],
                        boldFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                        boldColor: UIColor = UIColor.blue,
                        inString string: String,
                        font: UIFont = UIFont.systemFont(ofSize: 14),
                        color: UIColor = UIColor.black) -> NSAttributedString {
            let attributedString =
                NSMutableAttributedString(string: string,
                                        attributes: [
                                            NSAttributedString.Key.font: font,
                                            NSAttributedString.Key.foregroundColor: color])
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont, NSAttributedString.Key.foregroundColor: boldColor]
            for bold in strings {
                attributedString.addAttributes(boldFontAttribute, range: (string as NSString).range(of: bold))
            }
            return attributedString
        }

    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func subString(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }
    
    func subString(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }
    
    func subStringRange(from: Int, to: Int) -> String {
        let fromIndex = index(from: from)
        let toIndex = index(from: to)
        if to > from {
            return String(self[fromIndex..<toIndex])
        } else {
            return ""
        }
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
}

extension String {
    var length:Int {
        return self.count
    }
    
    func indexOf(target: String) -> Int? {
        
        let range = (self as NSString).range(of: target)
        
        guard Range(range) != nil else {
            return nil
        }
        
        return range.location
        
    }
    
    func lastIndexOf(target: String) -> Int? {
        let range = (self as NSString).range(of: target, options: NSString.CompareOptions.backwards)
        
        guard Range(range) != nil else {
            return nil
        }
        
        return range.location
    }
    
    func contains(s: String) -> Bool {
        return (self.range(of: s) != nil) ? true : false
    }
    
    func frame(size: CGSize, font: UIFont) -> CGRect {
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: self).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: font], context: nil)
    }
    
}

extension Int {
    
    var ordinal: String {
        var suffix: String
        let ones: Int = self % 10
        let tens: Int = (self/10) % 10
        if tens == 1 {
            suffix = "th"
        } else if ones == 1 {
            suffix = "st"
        } else if ones == 2 {
            suffix = "nd"
        } else if ones == 3 {
            suffix = "rd"
        } else {
            suffix = "th"
        }
        return "\(self)\(suffix)"
    }
    
    var stringValue: String {
        return "\(self)"
    }
}

extension Double {
    var stringWithoutZero: String {
        return truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

final class Initializer<T: UIView> {
    static func initialize(configure: (T) -> Void) -> T {
        let t = T()
        configure(t)
        return t
    }
}

extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}

//Akshat's extension to alert builder for custom message title
//Akshat's extension to alert builder for custom message title and custom actions

extension AlertBuilder {
    func buildMessageWithCustomTitle(vc: UIViewController, message: String, title: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        //alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        vc.present(alert, animated: true)
    }
    
    func buildMessageWithMulipleCallbacksAndCustomTitle(vc: UIViewController, message: String, title: String, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
        
        for action in actions {
            alert.addAction(action)
        }
        
        vc.present(alert, animated: true)
    }
}

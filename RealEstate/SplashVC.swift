//
//  ViewController.swift
//  RealEstate
//
//  Created by Muhammad Umair on 18/05/2020.
//  Copyright © 2020 Code Gradients. All rights reserved.
//

import UIKit
import Firebase

class SplashVC: UIViewController {
    
    public static var user_property_count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        do { try Auth.auth().signOut() } catch {}
        let first = UserDefaults.standard.bool(forKey: "initial_new")
        if first {
            if let user = Auth.auth().currentUser {
                Constants.mineId = user.uid
                
                Database.database().reference().child("properties").queryOrdered(byChild: "user").queryEqual(toValue: Constants.mineId).observeSingleEvent(of: .value) { (sn) in
                    SplashVC.user_property_count = Int(sn.childrenCount)
                }

                let newUser = UserDefaults.standard.bool(forKey: "is_new_user")
                if newUser {
                    let vc = AppStoryboard.AddProp.shared.instantiateViewController(withIdentifier: NewAddPropVC.storyboard_id)
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                } else {
                    let vc = AppStoryboard.Main.shared.instantiateViewController(withIdentifier: MainVC.storyboard_id)
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(3000)) {
                    let vc = AppStoryboard.Auth.shared.instantiateViewController(withIdentifier: LoginVC.storyboard_id)
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            }
        } else {
            let intro = AppStoryboard.Utils.shared.instantiateViewController(withIdentifier: IntroVC.storyboard_id)
            intro.modalPresentationStyle = .fullScreen
            self.present(intro, animated: true, completion: nil)
        }
    }
    
}


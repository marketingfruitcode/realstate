//
//  CustomPhotoAlbum.swift
//  RealEstate
//
//  Created by CodeGradients on 20/08/2020.
//  Copyright © 2020 Code Gradients. All rights reserved.
//

import Foundation
import UIKit
import Photos

class CustomPhotoAlbum {
    var albumName = "Rental Property Dashboard"
    init() {
        
    }
    func saveImage(image: UIImage, completation: @escaping (_ success: Bool) -> Void) {
        var assetCollection: PHAssetCollection!
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", self.albumName)
        var collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        if collection.count <= 0 {
            PHPhotoLibrary.shared().performChanges({
                PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: self.albumName)
            }) { success, _ in
                if success {
                    collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
                    if let _: AnyObject = collection.firstObject {
                        assetCollection = collection.firstObject
                        self.saveLogoImage(image: image, collection: assetCollection, completation: {(success) -> Void in
                            completation(success)
                        })
                    }
                } else {
                    print("Image will not be saved")
                }
            }
        } else {
            if let _: AnyObject = collection.firstObject {
                assetCollection = collection.firstObject
                self.saveLogoImage(image: image, collection: assetCollection, completation: {(success) -> Void in
                    completation(success)
                })
            }
        }
    }
    fileprivate func saveLogoImage(image: UIImage, collection: PHAssetCollection, completation: @escaping (_ success: Bool) -> Void) {
        PHPhotoLibrary.shared().performChanges({
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: collection)
            albumChangeRequest?.addAssets([assetPlaceholder] as NSFastEnumeration)
        }) { (_, error) in
            if error == nil {
                completation(true)
            } else {
                completation(false)
            }
        }
    }
}

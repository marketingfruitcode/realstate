//
//  Parser.swift
//  RealEstate
//
//  Created by Pritam Singh on 16/05/21.
//  Copyright © 2021 Code Gradients. All rights reserved.
//

import Foundation
//
//  DynamicValue.swift
//  Task
//
//  Created by Pritam Singh  on 04/03/20.
//  Copyright © 2020 Pritam Singh . All rights reserved.
//

import UIKit
//import Reachability


//MARK: - BASE URL

let BASE_URL = "https://api.mashvisor.com/v1.1/client/"



let api_purchase_info  = "\(BASE_URL)city/properties/"
let api_property_info  = "\(BASE_URL)property/"
protocol ParserManager {
    var methodType : String { get set }
    var url : String{ get set }
}
struct Parser: ParserManager{
    var methodType: String = "GET"
    var url: String = ""
    var observation: NSKeyValueObservation?

    var parserId = 0
     var task : URLSessionTask?
    func fetchData(completionHandler: @escaping (_ result: NSArray, _ statusCode : Int, _ error: Error?) -> Void) {
     
        guard let serviceUrl = URL(string: url) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "GET"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
//        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.addValue("e875e17a-7389-4454-aa16-6e413eccbac8", forHTTPHeaderField: "x-api-key")
        let session = URLSession.shared
         session.dataTask(with: request) { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {

            if let response = response {
//                // print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
//                     print(json)
                    DispatchQueue.main.async{
                        if let dict = json as? NSDictionary{
                            
                            completionHandler(NSArray(object: dict),httpResponse.statusCode ,error)
                        }else{
                            completionHandler(json as! NSArray ,httpResponse.statusCode,error)
                        }
                    }
                } catch {
                    // print(error)
//                    ToastManager.showToast("Opps something went wrong please try again.",duration: 5)
                    if data != nil {
                        let string = String(data: data, encoding: String.Encoding.utf8)
                         print(string)
                        completionHandler( NSArray() ,httpResponse.statusCode,error)
                    }
                }
            }
        }
      }.resume()
    }
    func postRequest() {
        // Create url object
        let urlString = "https://staging-py.producttube.com/api/v1/fieldings/store-list/?limit=10&offset=0&search="
        guard let url = URL(string: urlString ) else {return}
        
        // Create the session object
        let session = URLSession.shared
        
        // Create the URLRequest object using the url object
        var request = URLRequest(url: url)
        
        // Set the request method. Important Do not set any other headers, like Content-Type
        request.httpMethod = "POST" //set http method as POST
        
        // Set parameters here. Replace with your own.
//        let postData = "limit=100&offset=1&search=som".data(using: .utf8)
        let parameters: [String: Any] = [
//            "limit": "100",
//            "offset": "1",
//            "search": "Jack & Jill",
            "project_id": "490",
            "video_no": "1"
        ]
//        request.httpBody = parameters.percentEncoded()
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

//        self.getNormalHeader(&request)
        
//        let model = LoadTokenModel().loadTokenFromDisk()
//        let token = model.getAccessToken()
        // print(token)
//        if (token != ""){
//            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
//        }
        // Create a task using the session object, to run and return completion handler
        let webTask = session.dataTask(with: request, completionHandler: {data, response, error in
            guard error == nil else {
                // print(error?.localizedDescription ?? "Response Error")
                return
            }
            guard let serverData = data else {
                // print("server data error")
                return
            }
            do {
                if let requestJson = try JSONSerialization.jsonObject(with: serverData, options: .mutableContainers) as? [String: Any]{
//                    // print("Response: \(requestJson)")
                }
            } catch let responseError {
                // print("Serialisation in error in creating response body: \(responseError.localizedDescription)")
                let message = String(bytes: serverData, encoding: .ascii)
                // print(message as Any)
            }
        })
            // Run the task
            webTask.resume()
        
        }

    
  
}


extension String {
    func removeSpaces() -> String {
        guard let index = firstIndex(where: { !CharacterSet(charactersIn: String($0)).isSubset(of: .whitespaces) }) else {
            return self
        }
        return String(self[index...])
    }
}
extension NSMutableData {
    func appendString(_ string: String) {
        if let data = string.data(using: .utf8) {
            self.append(data)
        }
    }
}
extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}


//private let log = OSLog(subsystem: Bundle.main.bundleIdentifier!, category: #file)

class BackgroundSession: NSObject {
    var savedCompletionHandler: (() -> Void)?
    
    static var shared = BackgroundSession()
    
    private var session: URLSession!
    
    private override init() {
        super.init()

        let identifier = Bundle.main.bundleIdentifier! + ".backgroundSession"
        let configuration = URLSessionConfiguration.background(withIdentifier: identifier)

        session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }
}

extension BackgroundSession {
    @discardableResult
    func startUpload(for request: URLRequest, from data: Data) throws -> URLSessionUploadTask {
//        os_log("%{public}@: start", log: log, type: .debug, #function)

        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory())
            .appendingPathComponent(UUID().uuidString)
        try data.write(to: fileURL)
        let task = session.uploadTask(with: request, fromFile: fileURL)
        task.resume()

        return task
    }
}

extension BackgroundSession: URLSessionDelegate {
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
//        os_log(#function, log: log, type: .debug)

        DispatchQueue.main.async {
            self.savedCompletionHandler?()
            self.savedCompletionHandler = nil
        }
    }
}

extension BackgroundSession: URLSessionTaskDelegate {
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let error = error {
//            os_log("%{public}@: %{public}@", log: log, type: .error, #function, error.localizedDescription)
            return
        }
//        os_log("%{public}@: SUCCESS", log: log, type: .debug, #function)
    }
}

extension BackgroundSession: URLSessionDataDelegate {
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask) {
//        os_log(#function, log: log, type: .debug)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
//        os_log("%{public}@: received %d", log: log, type: .debug, #function, data.count)
    }
}
